# Sparql Query Rewriter #

Simple tool that adds access control to sparql queries

### Requirements ###

* Node.js installed (latest stable, v6.9.5)

### Use ###
In terminal: 

* Run `npm install` to install the required dependencies

* Run `npm test` to run the tests