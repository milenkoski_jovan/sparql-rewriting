module.exports = [
    {
        input: 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> ' +
        'PREFIX entx: <http://entx.com/>' +
        'SELECT ?id ?name ?salary WHERE { GRAPH entx:EmployeeDetails { ?id foaf:name ?name. ?id entx:salary ?salary } }',
        output: 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> ' +
        'PREFIX entx: <http://entx.com/>' +
        'SELECT ?id ?name ?salary WHERE { GRAPH entx:EmployeeDetails { ?id foaf:name ?name. ?id entx:salary ?salary FILTER NOT EXISTS {GRAPH entx:EmployeeDetails { ?id foaf:name ?name. ?id entx:salary ?salary FILTER ( ?id = <http://entx.com/MRyan> ) } } } }'
    },
    {
        input: 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> ' +
        'PREFIX entx: <http://entx.com/>' +
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' +
        'SELECT ( COUNT(?id) AS ?numEmployees ) ( AVG(?salary) AS ?avgSalary ) WHERE { GRAPH entx:EmployeeDetails  { ?id rdf:type foaf:Person . ?id entx:salary ?salary} }',
        output: 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> ' +
        'PREFIX entx: <http://entx.com/>' +
        'PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>' +
        "SELECT ( COUNT(?id) AS ?numEmployees ) ( AVG(?salary) AS ?avgSalary ) WHERE { GRAPH entx:EmployeeDetails { ?id rdf:type foaf:Person . ?id entx:salary ?salary FILTER NOT EXISTS { GRAPH entx:EmployeeDetails { ?id rdf:type foaf:Person . ?id entx:salary ?salary FILTER ( ?id = entx:MRyan ) } } } }"
    },
    {
        input: 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> ' +
        'PREFIX entx: <http://entx.com/>' +
        'SELECT DISTINCT ?employee ?manager WHERE { GRAPH ?g { ?x foaf:name ?employee . ?y foaf:name ?manager { SELECT ?x ?y WHERE { GRAPH ?g { ?x entx:worksFor ?y } } } } }',
        output: 'PREFIX foaf: <http://xmlns.com/foaf/0.1/> ' +
        'PREFIX entx: <http://entx.com/>' +
        'SELECT DISTINCT ?employee ?manager WHERE { GRAPH ?g { ?x foaf:name ?employee . ?y foaf:name ?manager { SELECT ?x ?y WHERE { GRAPH ?g { ?x entx:worksFor ?y FILTER NOT EXISTS { GRAPH ?g { ?x entx:worksFor ?y FILTER ( ?x = entx:MRyan ) } } } } } } }'
    }
]
