var test = require('blue-tape');
var examples = require('./examples');
var Promise = require('bluebird');

var rewrite = require('../index').rewrite;
var format = require('../index').format;

// ⟨?sub, SELECT, −, ⟨entx:MRyan, entx:salary, ?o, ?g⟩, E, bsbm:Admin⟩ 
// ⟨?sub, SELECT, −, ⟨entx:MRyan, entx:worksFor, ?o, ?g⟩, E, bsbm:Admin⟩
var authPatterns = [
    {
        for: '?sub',
        queryType: 'SELECT',
        permission: 'deny',
        subject: 'http://entx.com/MRyan',
        predicate: 'http://entx.com/salary',
        object: '?salary',
        exclusion: 'bsbm:Admin'
    },
    {
        for: '?sub',
        queryType: 'SELECT',
        permission: 'deny',
        subject: 'http://entx.com/MRyan',
        predicate: 'http://entx.com/worksFor',
        object: '?o',
        exclusion: 'bsbm:Admin'
    }
];

examples.forEach((example, index) => {
    test(`example ${index + 1}`, t => {
        var result = rewrite(example.input, authPatterns);
        var expectedResult = format(example.output);
        console.log("\nRESULT\n");
        console.log(result);
        console.log("\nEXPECTED RESULT\n");
        console.log(expectedResult)
        t.equals(result, expectedResult, "Query OK");
        return Promise.resolve();
    });
})



