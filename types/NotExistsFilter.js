module.exports = function (auth, quad, basicPatterns) {
    var patterns = [];

    patterns.push(basicPatterns[0]);

    patterns.push({
        type: "filter",
        expression: {
            type: "operation",
            operator: "=",
            args: [
                quad.subject,
                auth.subject
            ]
        }
    });

    this.type = "filter";
    this.expression = {
        type: "operation",
        operator: "notexists",
        args: [
            {
                type: "graph",
                patterns: patterns,
                name: quad.graph
            }
        ]
    }

};