var SparqlParser = require('sparqljs').Parser;
var SparqlGenerator = require('sparqljs').Generator;
var parser = new SparqlParser();
var generator = new SparqlGenerator();
var _ = require('lodash');
var NotExistsFilter = require('./types/NotExistsFilter');

exports.rewrite = (query, authPatterns) => {

    var parsedQuery = parser.parse(query);

    parsedQuery.where.forEach(gp => {
        newgp = checkGP(gp, authPatterns);
        if (gp != newgp) {
            gp = newgp;
        }
    });

    return generator.stringify(parsedQuery);

}

function checkGP(gp, authPatterns) {
    // this check is for subqueries
    if (gp.where) {
        gp = gp.where[0];
    }

    var patterns = gp.patterns || gp.where[0].patterns;
    patterns.forEach(subElement => {
        // TODO check for exists filter, not exists filter, minus operator
        if (subElement.type == "query") {
            checkGP(subElement, authPatterns);
        }
    });

    gp = checkAuth(gp, authPatterns);

    return gp;
}

function checkAuth(gp, authPatterns) {
    let graphName = gp.name;
    gp.patterns[0].triples = gp.patterns[0].triples.map(triple => _.extend(triple, { graph: graphName }));

    gp.patterns[0].triples.forEach(quad => {
        authPatterns.forEach(auth => {
            if (match(quad, auth)) {
                var filter = new NotExistsFilter(auth, quad, gp.patterns);
                gp.patterns.push(filter);
            }
        });
    });

    return gp;
}

// not sure about this
function match(quad, auth) {
    return quad.predicate === auth.predicate;
}

exports.format = (query) => {
    var parsedQuery = parser.parse(query);
    return generator.stringify(parsedQuery);
}
